using System.IO;
using System.Reflection;
using Android.App;
using Plugin.EmbeddedResource;
using PortableRazor.Utilities;

namespace PortableRazor.Andorid
{
    public class ResourceManager : BaseResourceManager
    {
        public ResourceManager()
        {
            Logging.Register(new LogWriter());

            // default path
            ContentPath = $"/data/data/{Application.Context.PackageName}/files/";
        }

        protected override void ExtractResource(Assembly assembly, string filePath, string resource)
        {
            // in case files change, this is not a good idea
            // todo: find somekind of timestamp or hash check (if that is cheaper)
            // if (File.Exists(filePath))
            //     return;

            // create the folder if not existing
            filePath = filePath.Replace("\\", "/");
            var directoryName = filePath.Substring(0, filePath.LastIndexOf("/"));
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            // open embedded file stream and target file stream
            var input = ResourceLoader.GetEmbeddedResourceStream(assembly, resource);
            var output = new FileStream(filePath, FileMode.OpenOrCreate);

            // copy input to output
            input.CopyTo(output);
            output.Flush();

            // close streams
            output.Dispose();
            input.Dispose();
        }
    }
}
