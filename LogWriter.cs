using Android.Util;
using PortableRazor.Utilities;

namespace PortableRazor.Andorid
{
    internal class LogWriter : ILogWriter
    {
        public void Print(LogLevel level, string message)
        {
            switch (level)
            {
                case Utilities.LogLevel.Error:
                    Log.Error("PortableRazor", $"[{level.ToString()}] {message}");
                    break;

                case Utilities.LogLevel.Warning:
                    Log.Warn("PortableRazor", $"[{level.ToString()}] {message}");
                    break;

                case Utilities.LogLevel.Info:
                    Log.Info("PortableRazor", $"[{level.ToString()}] {message}");
                    break;

                case Utilities.LogLevel.Verbose:
                    Log.Verbose("PortableRazor", $"[{level.ToString()}] {message}");
                    break;
            }
        }
    }
}