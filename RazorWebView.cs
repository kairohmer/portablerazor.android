using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Webkit;
using Java.Interop;
using PortableRazor.Utilities;
using Object = Java.Lang.Object;

namespace PortableRazor.Andorid
{
    public class RazorWebView : WebView, IHybridWebView
    {
        /// <summary>
        /// The context of this webview.
        /// It's the main API access point that offers pretty much everything (that is implemented).
        /// </summary>
        public PortableRazorContext RazorContext { get; private set; }

        /// <summary>
        /// Invoked from the java script code.
        /// This event is processed by the Context.
        /// </summary>
        public event EventHandler<string> NotifyFromJavascript;

        /// <summary>
        /// Invoked during naviation to load a new controller / action.
        /// This event is processed by the Context.
        /// </summary>
        public event EventHandler<RoutingEventArgs> RoutingRequest;

        /// <summary>
        /// Log for this object.
        /// </summary>
        private readonly Logging _Log = new Logging("RazorWebView");

        private Activity _Activity = null;


        public RazorWebView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Initialize(null);
        }

        public RazorWebView(Context context) : base(context)
        {
            Initialize(context);
        }

        public RazorWebView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize(context);
        }

        public RazorWebView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Initialize(context);
        }

        public RazorWebView(Context context, IAttributeSet attrs, int defStyleAttr, bool privateBrowsing) : base(context, attrs, defStyleAttr, privateBrowsing)
        {
            Initialize(context);
        }

        public RazorWebView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            _Activity = context as Activity;
            if (_Activity == null)
                _Log.Error("Activity Context is null");

            RazorContext = new PortableRazorContext(this);

            SetBackgroundColor(Color.Transparent);

            // Use subclassed WebViewClient to intercept hybrid native calls
            var webViewClient = new HybridWebViewClient(this);

            SetWebViewClient(webViewClient);
            Settings.CacheMode = CacheModes.CacheElseNetwork;
            Settings.JavaScriptEnabled = true;
            SetWebChromeClient(new HybridWebChromeClient(Context, this));

            AddJavascriptInterface(new JavaScriptInterface(this), "external");
        }

        public string BasePath => System.IO.Path.Combine(BaseResourceManager.ContentPath, "www");


        public async void LoadHtmlString(string html)
        {
            try
            {
                await Threading.DispatchToGuiThreadAsync(_Activity, () =>
                {
                    var url = "file://" + BasePath + "/";
                    LoadDataWithBaseURL(url, html, "text/html", "UTF-8", null);
                });

            }
            catch (Exception ex)
            {
                _Log.Error("Failed to load html from string.", ex);
            }
        }

        public async Task<string> EvaluateJavascriptAsync(string script)
        {
            var result = new JavascriptRestult();
            result.Script = script; // for debugging

            // make sure the script is evaluated on the main thread
            // if not at least windows will throw an exception..
            await Threading.DispatchToGuiThreadAsync(_Activity, () =>
            {
                try
                {
                    // https://forums.xamarin.com/discussion/55398/webview-evaluatejavascript-return-value-with-ivaluecallback
                    EvaluateJavascript("javascript:" + script, result);
                   // LoadUrl("javascript:" + script);
                }
                catch (Exception ex)
                {
                    _Log.Error("Evaluating Javascript failed", ex);
                    //Debug.WriteLine($"Eval exception: {ex.Message}");
                    result.WaitHandle.Release();
                }
            });

            await result.WaitHandle.WaitAsync();
            return result.Value;
        }

        internal class JavascriptRestult : Java.Lang.Object, IValueCallback
        {
            internal string Value = null;
            internal string Script = null;
            internal SemaphoreSlim WaitHandle = new SemaphoreSlim(0, 1);
            public void OnReceiveValue(Object value)
            {
                if (value != null)
                {
                    Value = Convert.ToString(value);
                    if(Value.StartsWith("\"") && Value.EndsWith("\""))
                        Value = Value.Substring(1, Value.Length - 2);
                }

                WaitHandle.Release();
            }
        }

        private class JavaScriptInterface : Java.Lang.Object
        {
            private readonly RazorWebView _WebView;

            public JavaScriptInterface(RazorWebView webView)
            {
                _WebView = webView;
            }

            [JavascriptInterface]
            [Export("notify")]
            public void Notify(Java.Lang.String message)
            {
                var args = Convert.ToString(message);

                Task.Run(() =>
                {
                    try
                    {

                        _WebView.NotifyFromJavascript?.Invoke(this, args);
                    }
                    catch (Exception ex)
                    {
                        _WebView._Log.Error("Failed to handle call from Javascript.", ex);
                    }
                });
            }
        }


        private class HybridWebViewClient : WebViewClient
        {
            private readonly RazorWebView _WebView;

            public HybridWebViewClient(RazorWebView webview)
            {
                _WebView = webview;
            }

            public override bool ShouldOverrideUrlLoading(WebView webView, string url)
            {
                RoutingEventArgs routingArgs = new RoutingEventArgs(new Uri(url));
                _WebView.RoutingRequest?.Invoke(this, routingArgs);
                return routingArgs.Handled;
            }

            public override void OnReceivedError(WebView view, IWebResourceRequest request, WebResourceError error)
            {
                _WebView._Log.Error($"Webview Error [{error.ErrorCode}]: {error.Description}");
                base.OnReceivedError(view, request, error);
            }

            public override void OnReceivedHttpError(WebView view, IWebResourceRequest request, WebResourceResponse errorResponse)
            {
                _WebView._Log.Error($"Webview Error [{errorResponse.StatusCode}]: {errorResponse.ReasonPhrase}");
                base.OnReceivedHttpError(view, request, errorResponse);
            }

            public override void OnPageFinished(WebView view, string url)
            {
                //_WebView._Log.Info($"Page Finished: {url}");
                base.OnPageFinished(view, url);
            }
        }

        private class HybridWebChromeClient : WebChromeClient
        {
            private readonly Context _Context;
            private readonly RazorWebView _WebView;

            public HybridWebChromeClient(Context context, RazorWebView webView) : base()
            {
                this._Context = context;
                this._WebView = webView;
            }

            public override bool OnConsoleMessage(ConsoleMessage consoleMessage)
            {
                var level = LogLevel.Verbose;
                if(consoleMessage.InvokeMessageLevel() == ConsoleMessage.MessageLevel.Error)    level = LogLevel.Error;
                if(consoleMessage.InvokeMessageLevel() == ConsoleMessage.MessageLevel.Warning)    level = LogLevel.Warning;
                if(consoleMessage.InvokeMessageLevel() == ConsoleMessage.MessageLevel.Tip)    level = LogLevel.Info;
                if(consoleMessage.InvokeMessageLevel() == ConsoleMessage.MessageLevel.Log)    level = LogLevel.Info;
                if(consoleMessage.InvokeMessageLevel() == ConsoleMessage.MessageLevel.Debug)    level = LogLevel.Verbose;

                _WebView._Log.Write(level, $"[{consoleMessage.SourceId()} Line:{consoleMessage.LineNumber()}] {consoleMessage.Message()}");
                return base.OnConsoleMessage(consoleMessage);
            }

            public override bool OnJsAlert(WebView view, string url, string message, JsResult result)
            {
                var alertDialogBuilder = new AlertDialog.Builder(_Context)
                    .SetMessage(message)
                    .SetCancelable(false)
                    .SetPositiveButton("Ok", (sender, args) => {
                        result.Confirm();
                    });

                alertDialogBuilder.Create().Show();

                return true;
            }

            public override bool OnJsConfirm(WebView view, string url, string message, JsResult result)
            {
                var alertDialogBuilder = new AlertDialog.Builder(_Context)
                    .SetMessage(message)
                    .SetCancelable(false)
                    .SetPositiveButton("Ok", (sender, args) => {
                        result.Confirm();
                    })
                    .SetNegativeButton("Cancel", (sender, args) => {
                        result.Cancel();
                    });

                alertDialogBuilder.Create().Show();

                return true;
            }
        }
    }
}