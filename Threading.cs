﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;

namespace PortableRazor.Andorid
{
    internal static class Threading
    {
        public static bool IsRunningOnGuiThread => SynchronizationContext.Current != null;

        /// <summary>
        /// Send an action to perform on the main (UI) thread.
        /// </summary>
        /// <param name="action">code to execute</param>
        public static void DispatchToGuiThread(Activity activity, Action action)
        {
            try
            {
                if (IsRunningOnGuiThread)
                {
                    action();
                    return;
                }

                activity.RunOnUiThread(action);
            }
            catch (OperationCanceledException)
            {
                // this fine if the process was stopped from the outside
            }
            catch (Exception ex)
            {
                throw new Exception("[DispatchToGuiThread] Dispatch Failed: " + ex.Message);
            }
        }


        /// <summary>
        /// Send an action to perform on the main (UI) thread.
        /// </summary>
        /// <param name="action">code to execute</param>
        public static async Task DispatchToGuiThreadAsync(Activity activity, Action action)
        {
            try
            {
                if (IsRunningOnGuiThread)
                {
                    action();
                    return;
                }

                SemaphoreSlim locking = new SemaphoreSlim(0, 1);
                activity.RunOnUiThread(() =>
                {
                    action();
                    locking.Release();
                });

                await locking.WaitAsync();
            }
            catch (OperationCanceledException)
            {
                // this fine if the process was stopped from the outside
            }
            catch (Exception ex)
            {
                throw new Exception("[DispatchToGuiThread] Dispatch Failed: " + ex.Message);
            }
        }


        public static int GetThreadId()
        {
            return Environment.CurrentManagedThreadId;
        }
    }

}
